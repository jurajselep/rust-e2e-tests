use reqwest;

use serde::Deserialize;
use serde_json;

use chrono::{DateTime, Utc};
// use chrono::format::ParseError;

use std::process::Command;

use slack_hook::{PayloadBuilder, Slack};

use ws::{connect, CloseCode};

#[derive(Debug, Deserialize)]
struct Bootstrapped {
    block: String,
    timestamp: String,
}

use std::thread;
use std::time::Duration;

// get last applied block timestamp
#[allow(dead_code)]
fn get_monitor_bootstrapped() -> Result<(), reqwest::Error> {
    // get data about progress of bootstrapping
    let response: String = reqwest::get("http://127.0.0.1:3030/monitor/bootstrapped")?.text()?;
    // reqwest::get("https://zeronet.simplestaking.com:3000/monitor/bootstrapped")?.text()?;
    // reqwest::get("https://alphanet.simplestaking.com:3000/monitor/bootstrapped")?.text()?;

    println!("[+][monitor] response {:?}", response);

    // deserialize data
    //let json: serde_json::Value =
    let response_node: Bootstrapped =
        serde_json::from_str(&response).expect("JSON was not well-formatted");

    // parse timestamp to int form request
    let datetime_node = DateTime::parse_from_rfc3339(&response_node.timestamp.to_string()).unwrap();

    // get current timestamp
    let datetime_system = Utc::now();

    // get duration in sec since last block
    let duration = datetime_system.signed_duration_since(datetime_node);

    println!(
        "[+][monitor][bootstrapped] Duration from last block | Days: {:?} | Hours: {:?} | Mins: {:?} | Secs: {:?} | {:?}",
        duration.num_days(),
        duration.num_hours(),
        duration.num_minutes(),
        duration.num_seconds(),
        duration
    );

    Ok(())
}

// spawn process and report process once it exits
#[allow(dead_code)]
fn spawn_process() {
    let tezos_path = "/home/xaxa/Projects/tezedge";
    // simulate exit
    let mut child_process = Command::new("bash")
        .arg("-c")
        // TODO: refactor temporary hack
        .arg(format!("rm -rf \"/tmp/tezos-ocaml-db\" & rm -rf \"/tmp/tezos-rust-db\" & mkdir \"/tmp/tezos-ocaml-db\" & mkdir \"/tmp/tezos-rust-db\" &\
            export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:{}/tezos/interop/lib_tezos/artifacts & echo $LD_LIBRARY_PATH &\
            {}/target/release/light-node --tezos-data-dir=\"/tmp/tezos-ocaml-db\"  --bootstrap-db-path=\"/tmp/tezos-rust-db\" --network=\"babylon\" --protocol-runner=\"{}/target/debug/protocol-runner\"",tezos_path,tezos_path,tezos_path))
        .spawn()
        .unwrap();

    // wait for process to exit
    match child_process.try_wait() {
        Ok(Some(status)) => println!("[-][exit] exited with: {}", status),
        Ok(None) => {
            println!("[+][exit] status not ready yet, let's really wait");
            let res = child_process.wait_with_output();
            println!("[-][exit] result: {:?}", res);
        }
        Err(e) => println!("[-][exit][error] attempting to wait: {}", e),
    }
}

// send message to slack via webhook
#[allow(dead_code)]
fn send_message_slack() {
    // slack webhook url
    let slack =
        Slack::new("https://hooks.slack.com/services/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")
            .unwrap();

    // create message for slack
    let payload = PayloadBuilder::new()
        .text("```delelte```")
        .channel("#tezedge")
        .username("[e2e][error]")
        .icon_emoji(":warning:")
        .build()
        .unwrap();

    // send message
    let resposne = slack.send(&payload);
    match resposne {
        Ok(()) => println!("[+][slack] ok"),
        Err(e) => println!("[-][slack][error]: {:?}", e),
    }
}

// receive messages from node
#[allow(dead_code)]
fn receive_node_metrics() {
    // connect to metrics
    connect("ws://demos.kaazing.com/echo", |out| {
        // send message to ws
        out.send("Hello WebSocket").unwrap();

        // process messages from ws
        move |msg| {
            println!("[ws][on_message] message: {}", msg);
            out.close(CloseCode::Normal)
        }
    })
    .unwrap()
}

fn main() {
    // check bootstrap status every 5 seconds
    // create new thread
    thread::spawn(|| {
        loop {
            // get last applied block timestamp
            if let Ok(()) = get_monitor_bootstrapped() {
                println!("[-][monitor]");
            }

            // wait for 3 sec
            thread::sleep(Duration::from_secs(1));
        }
    });

    // wait for process to exit and report error
    spawn_process();

    // send message to slack tezedge channel
    // send_message_slack();

    // connect to ws metrics
    // receive_node_metrics();

    // test concurent threads
}
