# Rust e2e tests

- launch process from command line 
- read file 
- read Rest API https://zeronet.simplestaking.com:3000/monitor/bootstrapped 
- report to slack
- run docker container

# Requirements
```
export OPENSSL_INCLUDE_DIR="$(brew --prefix openssl)/include"
export OPENSSL_LIB_DIR="$(brew --prefix openssl)/lib"
cargo clean
```


# Links for integration testing and reporting
- default integration testing
https://doc.rust-lang.org/rust-by-example/testing/integration_testing.html

- rust test output formats
https://internals.rust-lang.org/t/past-present-and-future-for-rust-testing/6354/103

- code coverage reports sent to codecov.io
https://users.rust-lang.org/t/test-coverage-with-grcov/24851/14

- transfor test report to junit format
https://www.reddit.com/r/rust/comments/apl8k6/tool_to_transform_your_test_reports_into_junit/

- integration tests discussion
https://www.reddit.com/r/rust/comments/cpg1ou/how_to_best_integration_test_a_webapp/

# Run tests
`cargo test`